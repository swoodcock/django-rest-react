from rest_framework import generics
from rest_framework.viewsets import ModelViewSet

from .models import Project, User
from .serializers import ProjectSerializer, UserSerializer


class ProjectListCreate(generics.ListCreateAPIView):
    """
    DRF view to CRUD Project model.
    """

    queryset = Project.objects.all()
    serializer_class = ProjectSerializer


class UserListCreate(generics.ListCreateAPIView):
    """
    DRF view to CRUD User model.
    """

    queryset = User.objects.all()
    serializer_class = UserSerializer


class ProjectViewSet(ModelViewSet):
    """
    Display data for Project model, in JSON format.
    """

    queryset = Project.objects.all()
    serializer_class = ProjectSerializer


class UserViewSet(ModelViewSet):
    """
    Display data CRUD User model, in JSON format.
    """

    queryset = User.objects.all()
    serializer_class = UserSerializer


class ProjectCreateView(generics.CreateAPIView):
    """
    Create a new instance of Project model.
    """

    queryset = Project.objects.all()
    serializer_class = ProjectSerializer


class ProjectDeleteView(generics.DestroyAPIView):
    """
    Create a new instance of Project model.
    """

    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
