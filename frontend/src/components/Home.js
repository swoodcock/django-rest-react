import React, { Component } from 'react'
import Table from 'react-bootstrap/Table'
import Container from 'react-bootstrap/Container'
import Button from 'react-bootstrap/Button'
const axios = require('axios');


export default class Home extends Component {
    state = {
        isLoading: true,
        project: [],
        error: null
    }

    constructor(props) {
        super(props);
        this.handleDelete = this.handleDelete.bind(this);
    }

    fetchEmp() {
        fetch(window.API_URL + `/api/project/`)
            .then(response => response.json())
            .then(data =>
               this.setState({
                   project: data,
                   isLoading: false,
               })
            )
           .catch(error => this.setState({ error, isLoading: false }));
    }

    componentDidMount() {
        this.fetchEmp();
    }

    handleDelete(id) {
        axios.delete(window.API_URL + `/api/project/${id}/delete`)
    }

    render() {
        return (
            <Container style={{ marginTop: '100px' }}>
            <Button variant="secondary" style={{ float: 'right', margin: '20px' }} onClick={() => this.props.history.push('/project/create/')}>Add a Project</Button>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Key</th>
                  <th>Name</th>
                  <th>Users</th>
                  <th>Action</th>
                </tr>
              </thead>
               <tbody>
                  {!this.state.isLoading?this.state.project.map((item)=>{
                  return (
                     <tr key={item.id}>
                       <td>{item.id}</td>
                       <td>{item.key}</td>
                       <td>{item.name}</td>
                       <td>{item.users}</td>
                       <td><Button onClick={() => this.props.history.push(`/project/${item.id}/update/`)}>Update</Button>
                       <Button variant="danger" onClick={()=>this.handleDelete(item.id)}>Delete</Button></td>
                     </tr>
                  )
                  })
                  :
                     "LOADING"
                  }
               </tbody>
            </Table>
         </Container>
        )
      }

}
