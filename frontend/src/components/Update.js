import React, { Component } from 'react'
import { Container , Form, Row, Col, Button} from 'react-bootstrap'

export default class Update extends Component {
    render() {
        return (
            <Container style={{ marginTop: '100px' }}>
                <h1>Update Project</h1>
                <Form style={{ margin: '50px' }}>
                    <Form.Row>
                        <Col>
                        <Form.Control placeholder="Project Key" value="dl_1"/>
                        </Col>
                        <Col>
                        <Form.Control placeholder="Project Name" value="Data downloading projected"/>
                        </Col>
                        <Col>
                        <Form.Control placeholder="Users" value="Smith,John Jones,Dave"/>
                        </Col>
                    </Form.Row>
                    <Button style={{ margin: '30px', float: 'right' }}>Update Project</Button>
                </Form>
            </Container>
        )
    }
}
