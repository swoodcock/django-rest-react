import React, { Component } from 'react'
import { Container , Form, Row, Col, Button} from 'react-bootstrap'

export default class Create extends Component {
    render() {
        return (
            <Container style={{ marginTop: '100px' }}>
                <h1>Add Project</h1>
                <Form style={{ margin: '50px' }}>
                    <Form.Row>
                        <Col>
                        <Form.Control placeholder="Project Key" />
                        </Col>
                        <Col>
                        <Form.Control placeholder="Project Name" />
                        </Col>
                        <Col>
                        <Form.Control placeholder="Project Users" />
                        </Col>
                    </Form.Row>
                    <Button style={{ margin: '30px', float: 'right' }}>Add Project</Button>
                </Form>
            </Container>
        )
    }
}
