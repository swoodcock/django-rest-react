import factory

from projects.models import Project, User


class ProjectFactory(factory.DjangoModelFactory):
    class Meta:
        model = Project

    key = factory.faker.Faker("word")
    name = factory.faker.Faker("job")


class UserFactory(factory.DjangoModelFactory):
    class Meta:
        model = User

    name_first = factory.faker.Faker("first_name")
    name_last = factory.faker.Faker("last_name")
    email = factory.faker.Faker("email")
    date_joined = factory.faker.Faker("date_this_year")
    date_left = factory.faker.Faker("date_this_month")
    projects = factory.RelatedFactory(ProjectFactory)
