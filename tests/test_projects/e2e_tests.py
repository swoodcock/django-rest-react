import json

import pytest
from model_bakery import baker

from projects.models import Project, User

pytestmark = [pytest.mark.e2e, pytest.mark.django_db]


class TestProjectEndpoints:

    endpoint = "/api/project/"

    def test_list(self, api_client):
        baker.make(Project, _quantity=3)

        response = api_client().get(self.endpoint)

        assert response.status_code == 200
        assert len(json.loads(response.content)) == 3

    def test_create(self, api_client):
        project = baker.prepare(Project)
        expected_json = {
            "key": project.key,
            "name": project.name,
        }

        response = api_client().post(self.endpoint, data=expected_json, format="json")

        assert response.status_code == 201
        assert json.loads(response.content) == expected_json

    def test_retrieve(self, api_client):
        project = baker.make(Project)
        expected_json = {
            "key": project.key,
            "name": project.name,
            "created_at": project.created_at,
        }
        url = f"{self.endpoint}{project.id}/"

        response = api_client().get(url)

        assert response.status_code == 200
        assert json.loads(response.content) == expected_json

    def test_update(self, rf, api_client):
        old_project = baker.make(Project)
        new_project = baker.prepare(Project)
        currency_dict = {
            "key": new_project.key,
            "name": new_project.name,
            "created_at": new_project.created_at,
        }

        url = f"{self.endpoint}{old_project.id}/"

        response = api_client().put(url, currency_dict, format="json")

        assert response.status_code == 200
        assert json.loads(response.content) == currency_dict

    @pytest.mark.parametrize(
        "field",
        [
            ("key"),
            ("name"),
            ("created_at"),
        ],
    )
    def test_partial_update(self, mocker, rf, field, api_client):
        project = baker.make(Project)
        project_dict = {
            "key": project.key,
            "name": project.name,
            "created_at": project.created_at,
        }
        valid_field = project_dict[field]
        url = f"{self.endpoint}{project.id}/"

        response = api_client().patch(url, {field: valid_field}, format="json")

        assert response.status_code == 200
        assert json.loads(response.content)[field] == valid_field

    def test_delete(self, mocker, api_client):
        project = baker.make(Project)
        url = f"{self.endpoint}{project.id}/"

        response = api_client().delete(url)

        assert response.status_code == 204
        assert Project.objects.all().count() == 0


class TestUserEndpoints:

    endpoint = "/api/user/"

    def test_list(self, api_client, uubb):
        client = api_client()
        uubb(3)
        url = self.endpoint
        response = client.get(url)

        assert response.status_code == 200
        assert len(json.loads(response.content)) == 3

    def test_create(self, api_client, uubb):
        client = api_client()
        t = uubb(1)[0]
        valid_data_dict = {
            "name_first": t.name_first,
            "name_last": t.name_last.code,
            "email": t.email,
            "date_joined": t.date_joined,
            "date_left": t.date_left,
            "projects": t.projects,
            "created_at": t.created_at,
        }

        url = self.endpoint

        response = client.post(url, valid_data_dict, format="json")

        assert response.status_code == 201
        assert json.loads(response.content) == valid_data_dict
        assert User.objects.last().link

    def test_retrieve(self, api_client, fub):
        u = fub()
        u = User.objects.last()
        expected_json = u.__dict__
        expected_json["name_first"] = u.name_first
        expected_json["name_last"] = u.name_last
        expected_json["email"] = u.email
        expected_json["date_joined"] = expected_json["date_joined"].strftime(
            "%Y-%m-%dT%H:%M:%S.%fZ"
        )
        expected_json["date_left"] = expected_json["date_left"].strftime(
            "%Y-%m-%dT%H:%M:%S.%fZ"
        )
        expected_json["projects"] = u.projects
        expected_json["created_at"] = expected_json["created_at"].strftime(
            "%Y-%m-%dT%H:%M:%S.%fZ"
        )

        url = f"{self.endpoint}{u.id}/"

        response = api_client().get(url)

        assert response.status_code == 200 or response.status_code == 301
        assert json.loads(response.content) == expected_json

    def test_update(self, api_client, uubb):
        old_user = uubb(1)[0]
        u = uubb(1)[0]
        expected_json = u.__dict__
        expected_json["name_first"] = u.name_first
        expected_json["name_last"] = u.name_last
        expected_json["email"] = u.email
        expected_json["date_joined"] = expected_json["date_joined"].strftime(
            "%Y-%m-%dT%H:%M:%S.%fZ"
        )
        expected_json["date_left"] = expected_json["date_left"].strftime(
            "%Y-%m-%dT%H:%M:%S.%fZ"
        )
        expected_json["projects"] = u.projects
        expected_json["created_at"] = expected_json["created_at"].strftime(
            "%Y-%m-%dT%H:%M:%S.%fZ"
        )

        url = f"{self.endpoint}{old_user.id}/"

        response = api_client().put(url, data=expected_json, format="json")

        assert response.status_code == 200 or response.status_code == 301
        assert json.loads(response.content) == expected_json

    @pytest.mark.parametrize(
        "field",
        [
            ("name_first"),
            ("name_last"),
            ("email"),
            ("date_joined"),
            ("date_left"),
            ("projects"),
            ("created_at"),
        ],
    )
    def test_partial_update(self, api_client, field, uubb):
        uubb(2)
        old_user = User.objects.first()
        new_user = User.objects.last()
        valid_field = {
            field: new_user.__dict__[field],
        }
        url = f"{self.endpoint}{old_user.id}/"

        response = api_client().patch(
            path=url,
            data=valid_field,
            format="json",
        )

        assert response.status_code == 200 or response.status_code == 301
        try:
            assert json.loads(response.content)[field] == valid_field[field]
        except json.decoder.JSONDecodeError:
            pass

    def test_delete(self, api_client, uubb):
        user = uubb(1)[0]
        url = f"{self.endpoint}{user.id}/"

        response = api_client().delete(url)

        assert response.status_code == 204 or response.status_code == 301
