#!/bin/bash
set -euo pipefail

python manage.py collectstatic --noinput

exec "$@"
