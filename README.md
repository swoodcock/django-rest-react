# **Complete 16/05/2021**
Simple React frontend, displaying data from DRF backend.
Extensive unit and e2e testing included.

Instructions (prod):
- Clone repo
- Edit .env and .runtime.env (& docker-compose.yml if required)
- Run `bash build_scrip.sh`
- Run `docker-compose up`

Instructions (dev):
- Run `./manage.py migrate`.
- In _frontend_ folder, install node_modules with `npm install`
- In _frontend_ folder, run **webpack** with `npm run dev`.
- In project home, run `./manage.py runserver`.
- Create some projects by accessing http://localhost:8000/api/project/.
- Head over http://127.0.0.1:8000/ to list the just created projects in your **React** client.



# Project details

This skeleton project has been generated following the instructions given [here](https://www.valentinog.com/blog/drf/).

It is a simple [django](https://www.djangoproject.com/) - [React](https://reactjs.org/) project, composed of a single
entity `Project`.

## Project goals

To each project, a list of users should be attached.

Technically, we want to vertically implement a `User` entity. A `User` is typically composed of following
properties: first/last name and email. To each project, it should be possible to attach some users.

What the project includes:

- On the _frontend_, we should be able to list each project with associated users
- Some tests
